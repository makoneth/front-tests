# Construye un Giphy client que busque perritos, de preferencia en Vue.js
- Responsivo.
- Utiliza componentes reutilizables.
- Maneja animaciones que le den ganas al usuario de regresar.
- Guardar Mis búsquedas anteriores en localstorage.
- Sube tu propuesta al repositorio como Merge Request.

Opcionalmente:
- Usa tailwind para definir los estilos de tu componentes.
